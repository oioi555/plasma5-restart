# Plasma5-restart
Clean cache to resolve upgrade problems.
This command may eliminate the problems caused by the KDE Plasma update.

[wiki.archlinux.org KDE Clean cache to resolve upgrade problems](https://wiki.archlinux.org/title/KDE#Clean_cache_to_resolve_upgrade_problems)

## Installation
```
git clone https://gitlab.com/oioi555/plasma5-restart.git
cd ./plasma5-restart
./install.sh
```
## Uninstallation
```
rm ~/.local/share/applications/Plasma5-restart.desktop
update-desktop-database ~/.local/share/applications
```
## How to use
[Alt] + [Space]  
Search by Krunner  

![output-2023-02-05_15.56.35](/uploads/7def5c666b8185d1ab86912d48130e97/output-2023-02-05_15.56.35.gif)

## License
[MIT License](LICENSE)

